# To SDF

```
conan install -if build . -s build_type=Debug --build=missing
conan install -if build . -s build_type=Release --build=missing
cmake -S . -B build
cmake --build build --config Debug
cmake --build build --config Release

```
