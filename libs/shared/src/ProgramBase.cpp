#include "ProgramBase.hpp"

#include "util.hpp"
#include "Log.hpp"

void ProgramBase::reload(const ShaderSourcePaths & shaderSourcePaths, std::string_view debugProgramName) {
    glDeleteProgram(mName);
    mName = glCreateProgram();
    const auto vertShader = attachAndReturnShader(GL_VERTEX_SHADER,          shaderSourcePaths.vert, debugProgramName);
    const auto tescShader = attachAndReturnShader(GL_TESS_CONTROL_SHADER,    shaderSourcePaths.tesc, debugProgramName);
    const auto teseShader = attachAndReturnShader(GL_TESS_EVALUATION_SHADER, shaderSourcePaths.tese, debugProgramName);
    const auto geomShader = attachAndReturnShader(GL_GEOMETRY_SHADER,        shaderSourcePaths.geom, debugProgramName);
    const auto fragShader = attachAndReturnShader(GL_FRAGMENT_SHADER,        shaderSourcePaths.frag, debugProgramName);
    const auto compShader = attachAndReturnShader(GL_COMPUTE_SHADER,         shaderSourcePaths.comp, debugProgramName);
    link(debugProgramName);
    if (mName != 0) {
        if (vertShader != 0) glDetachShader(mName, vertShader);
        if (tescShader != 0) glDetachShader(mName, tescShader);
        if (teseShader != 0) glDetachShader(mName, teseShader);
        if (geomShader != 0) glDetachShader(mName, geomShader);
        if (fragShader != 0) glDetachShader(mName, fragShader);
        if (compShader != 0) glDetachShader(mName, compShader);
    }
    glDeleteShader(vertShader);
    glDeleteShader(tescShader);
    glDeleteShader(teseShader);
    glDeleteShader(geomShader);
    glDeleteShader(fragShader);
    glDeleteShader(compShader);
}

GLuint ProgramBase::attachAndReturnShader(GLenum shaderType, const std::filesystem::path & shaderSourceFilePath, std::string_view debugProgramName) {
    if (shaderSourceFilePath.empty())
        return 0;
    const auto shaderSource = util::loadFile<GLchar>(shaderSourceFilePath);
    if (shaderSource.size() > std::numeric_limits<GLint>::max())
        return 0;
    const GLint shaderSourceLength = static_cast<GLint>(shaderSource.size());
    const auto shaderSourcePtr = shaderSource.data();
    const auto shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &shaderSourcePtr, &shaderSourceLength);
    glCompileShader(shader);
    GLint success = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (success == GL_FALSE) {
        GLint infoLogLength = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
        std::vector<GLchar> infoLog(infoLogLength);
        glGetShaderInfoLog(shader, infoLogLength, nullptr, infoLog.data());
        log::error("{} : {} : {}", debugProgramName, shaderSourceFilePath.string(), infoLog.data());
        glDeleteShader(shader);
        return 0;
    }
    glAttachShader(mName, shader);
    return shader;
}

void ProgramBase::link(std::string_view debugProgramName) {
    glLinkProgram(mName);
    GLint success = GL_FALSE;
    glGetProgramiv(mName, GL_LINK_STATUS, &success);
    if (success == GL_FALSE) {
        GLint infoLogLength = 0;
        glGetProgramiv(mName, GL_INFO_LOG_LENGTH, &infoLogLength);
        std::vector<GLchar> infoLog(infoLogLength);
        glGetProgramInfoLog(mName, infoLogLength, nullptr, infoLog.data());
        if (infoLog.size() < 1)
            infoLog.push_back('\0');
        log::error("{} : {}", debugProgramName, infoLog.data());
        glDeleteProgram(mName);
        mName = 0;
    }
}
