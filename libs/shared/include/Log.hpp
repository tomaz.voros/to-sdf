#pragma once

#include <iostream>
#include <sstream>

#include <fmt/format.h>

class log {
public:
    static inline constexpr auto DO_LOG = true;

    static inline constexpr auto DO_LOG_ERROR   = DO_LOG && true;
    static inline constexpr auto DO_LOG_WARNING = DO_LOG && true;
    static inline constexpr auto DO_LOG_INFO    = DO_LOG && true;
    static inline constexpr auto DO_LOG_DEBUG   = DO_LOG && true;

    template<typename ... Args> static void error  (Args && ... args) { if constexpr (DO_LOG_ERROR)   print("--[ERROR]---- ", args ...); }
    template<typename ... Args> static void warning(Args && ... args) { if constexpr (DO_LOG_WARNING) print("--[WARNING]-- ", args ...); }
    template<typename ... Args> static void info   (Args && ... args) { if constexpr (DO_LOG_INFO)    print("--[INFO]----- ", args ...); }
    template<typename ... Args> static void debug  (Args && ... args) { if constexpr (DO_LOG_DEBUG)   print("--[DEBUG]---- ", args ...); }

private:
    template <typename TypeString, typename... Args> static void print(const TypeString & typeString, Args &&... args) {
        std::ostringstream oss;
        oss << typeString;
        oss << fmt::format(args...);
        oss << '\n';
        std::cout << oss.str();
    }

};
