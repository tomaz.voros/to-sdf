#pragma once

#include <filesystem>
#include <vector>
#include <fstream>

namespace util {

template <typename T>
std::vector<T> loadFile(const std::filesystem::path & filePath) {
    std::vector<T> result;
    std::error_code errorCode;
    const auto fileSize = std::filesystem::file_size(filePath, errorCode);
    if (fileSize <= 0 || fileSize == static_cast<std::uintmax_t>(-1))
        return result;
    if (fileSize > std::numeric_limits<std::size_t>::max())
        return result;
    std::ifstream fileStream{ filePath, std::ifstream::in | std::ifstream::binary };
    if (!fileStream.good())
        return result;
    result.resize(static_cast<std::size_t>(fileSize) / sizeof(T));
    fileStream.read(reinterpret_cast<char *>(result.data()), result.size() * sizeof(T));
    if (!fileStream.good())
        result = {};
    return result;
}

}
