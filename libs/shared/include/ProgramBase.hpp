#pragma once

#include <filesystem>
#include <string_view>

#include <glad/glad.h>

class ProgramBase {
public:
    struct ShaderSourcePaths {
        std::filesystem::path vert;
        std::filesystem::path tesc;
        std::filesystem::path tese;
        std::filesystem::path geom;
        std::filesystem::path frag;
        std::filesystem::path comp;
    };

    ProgramBase() noexcept = default;
    ProgramBase(const ProgramBase &) = delete;
    ProgramBase & operator = (const ProgramBase &) = delete;
    ProgramBase(ProgramBase && o) noexcept : mName{ o.mName } { o.mName = 0; }
    ProgramBase & operator = (ProgramBase && o) noexcept { glDeleteProgram(mName); mName = o.mName; o.mName = 0; return *this; }

    GLuint name() const noexcept { return mName; }

protected:
    ~ProgramBase() noexcept { glDeleteProgram(mName); }
    void reload(const ShaderSourcePaths & shaderSourcePaths, std::string_view debugProgramName);

private:
    GLuint mName = 0;

    GLuint attachAndReturnShader(GLenum shaderType, const std::filesystem::path & shaderSourceFilePath, std::string_view debugProgramName);
    void link(std::string_view debugProgramName);

};
