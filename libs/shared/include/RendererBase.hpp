#pragma once

class RendererBase {
public:
    virtual void draw(float dt, float * VP) {}
    virtual void drawGui() {}
    virtual ~RendererBase() = default;
};
