set(TARGET imgui)

set(H_FILES
    include/imgui/imgui.h
    include/imgui/imgui_impl_glfw.h
    include/imgui/imgui_impl_opengl3.h
    include/imgui/imconfig.h
    src/imgui_internal.h
    src/imstb_textedit.h
    src/imstb_rectpack.h
    src/imstb_truetype.h
)

set(CPP_FILES
    src/imgui.cpp
    src/imgui_impl_glfw.cpp
    src/imgui_impl_opengl3.cpp
    src/imgui_widgets.cpp
    src/imgui_draw.cpp
    src/imgui_demo.cpp
)

add_library(${TARGET} ${H_FILES} ${CPP_FILES})

target_compile_features(${TARGET} PRIVATE cxx_std_17)

target_include_directories(${TARGET} PUBLIC include)
target_include_directories(${TARGET} PRIVATE include/imgui)

target_link_libraries(${TARGET} CONAN_PKG::glfw)

target_link_libraries(${TARGET} glad)
