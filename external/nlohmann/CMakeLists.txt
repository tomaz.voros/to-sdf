set(TARGET nlohmann)
set(TARGET_NAME nlohmann-sources)

set(HEADER_FILES
    include/nlohmann/json.hpp
)

add_library(${TARGET} INTERFACE)

target_compile_features(${TARGET} INTERFACE cxx_std_20)

set_source_files_properties(${HEADER_FILES} PROPERTIES LANGUAGE CXX)

add_custom_target(${TARGET_NAME} SOURCES ${HEADER_FILES})

target_include_directories(${TARGET} INTERFACE include)
