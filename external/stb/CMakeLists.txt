set(TARGET stb)

set(H_FILES
    include/stb/stb_image.h
    include/stb/stb_image_write.h
    include/stb/stb_truetype.h
)

set(CPP_FILES
    src/stb_image.cpp
    src/stb_image_write.cpp
    src/stb_truetype.cpp
)

add_library(${TARGET} ${H_FILES} ${CPP_FILES})

target_compile_features(${TARGET} PRIVATE cxx_std_17)

target_include_directories(${TARGET} PUBLIC include)
