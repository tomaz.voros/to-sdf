#version 460 core

layout (location = 0) uniform mat4 uVP;

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inColor;

out vec4 color;

void main() {
    gl_Position = vec4(inPosition, 1.0);
    color = vec4(inColor, 1.0);
}
