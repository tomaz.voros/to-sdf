#version 460 core

layout (location = 0) out vec4 outColor;

flat in vec2 lineP0;
flat in vec2 lineP1;
in vec2 P;
flat in float distRangeInv;

float pointLineSegmentDistance(vec2 p0, vec2 p1, vec2 p) {
    vec2 l0 = p1 - p0;
    vec2 l1 = p - p0;
    float n = dot(l0, l1);
    float d = dot(l0, l0);
    float t = n / d;
    vec2 nearest = clamp(t, 0.0, 1.0) * l0 + p0;
    return distance(p, nearest);
}

void main() {
    float dist = pointLineSegmentDistance(lineP0, lineP1, P);
    dist *= distRangeInv;
    dist = clamp(dist, 0.0, 1.0);
    outColor = vec4((1.0 - dist) * 0.5, 0.0, 0.0, 1.0);
    gl_FragDepth = dist;
}
