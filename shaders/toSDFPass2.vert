#version 460 core

layout (location = 0) uniform mat4 uVP;

layout (std430, binding = 0) buffer VertexData {
    float vertexData[];
} inVertexBuffer;

void main() {
    uint lineIndex = uint(gl_VertexID);
    vec2 p = {
        inVertexBuffer.vertexData[lineIndex * 4u + 0u],
        inVertexBuffer.vertexData[lineIndex * 4u + 1u]
    };
    gl_Position = vec4((uVP * vec4(p, 0.0, 1.0)).xy, 0.0, 1.0);
}
