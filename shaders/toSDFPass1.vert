#version 460 core

layout (location = 0) uniform mat4 uVP;
layout (location = 1) uniform float uDistanceRange;

layout (std430, binding = 0) buffer VertexData {
    float vertexData[];
} inVertexBuffer;

flat out vec2 lineP0;
flat out vec2 lineP1;
out vec2 P;
flat out float distRangeInv;

void getLinePos(out vec2 p0, out vec2 p1) {
    uint lineIndex = uint(gl_VertexID) / 6u;

    p0.x = inVertexBuffer.vertexData[lineIndex * 4u + 0u];
    p0.y = inVertexBuffer.vertexData[lineIndex * 4u + 1u];

    p1.x = inVertexBuffer.vertexData[lineIndex * 4u + 2u];
    p1.y = inVertexBuffer.vertexData[lineIndex * 4u + 3u];
}

vec2 getVertexPos(vec2 p0, vec2 p1) {
    uint quadVertexIndex = uint(gl_VertexID) % 6u;

    vec2 line = normalize(p1 - p0) * uDistanceRange;
    vec2 normal = { -line.y, line.x };

    switch (quadVertexIndex) {
        default:
        case 0:
            return p0 - line - normal;
        case 1:
            return p0 - line + normal;
        case 2:
        case 3:
            return p1 + line + normal;
        case 4:
            return p1 + line - normal;
    }
}

void main() {
    vec2 p0, p1;
    getLinePos(p0, p1);
    p0 = (uVP * vec4(p0, 0.0, 1.0)).xy;
    p1 = (uVP * vec4(p1, 0.0, 1.0)).xy;
    vec2 p = getVertexPos(p0, p1);
    gl_Position = vec4(p, 0.0, 1.0);
    P = p;
    lineP0 = p0;
    lineP1 = p1;
    distRangeInv = 1.0 / uDistanceRange;
}
