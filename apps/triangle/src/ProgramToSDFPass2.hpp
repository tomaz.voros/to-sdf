#pragma once

#include "ProgramBase.hpp"

class ProgramToSDFPass2 : public ProgramBase {
public:
    static constexpr GLint uVP = 0;
    static constexpr GLint inVertexBuffer = 0;

    void reload(const std::filesystem::path & assetPath) {
        ProgramBase::ShaderSourcePaths shaderSourcePaths;
        shaderSourcePaths.vert = assetPath / "shaders/toSDFPass2.vert";
        shaderSourcePaths.frag = assetPath / "shaders/toSDFPass2.frag";
        ProgramBase::reload(shaderSourcePaths, "ProgramToSDF");
    }

};
