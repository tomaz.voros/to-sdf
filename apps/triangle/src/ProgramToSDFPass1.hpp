#pragma once

#include "ProgramBase.hpp"

class ProgramToSDFPass1 : public ProgramBase {
public:
    static constexpr GLint uVP = 0;
    static constexpr GLint inVertexBuffer = 0;
    static constexpr GLint uDistanceRange = 1;

    void reload(const std::filesystem::path & assetPath) {
        ProgramBase::ShaderSourcePaths shaderSourcePaths;
        shaderSourcePaths.vert = assetPath / "shaders/toSDFPass1.vert";
        shaderSourcePaths.frag = assetPath / "shaders/toSDFPass1.frag";
        ProgramBase::reload(shaderSourcePaths, "ProgramToSDF");
    }

};
