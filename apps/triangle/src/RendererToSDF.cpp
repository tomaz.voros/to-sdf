#include "RendererToSDF.hpp"

#include <stdexcept>

#include <earcut/earcut.hpp>

RendererToSDF::RendererToSDF(
    const std::filesystem::path & assetPath
) {
    mShaderPass1.reload(assetPath);
    mShaderPass2.reload(assetPath);
}

void RendererToSDF::loadPolygon(
    const std::vector<std::vector<std::array<float, 2>>> & polygon
) {
    auto indices = mapbox::earcut<uint32_t>(polygon);

    std::vector<float> vertices; // TODO: reserve
    for (auto & ring : polygon) {
        for (size_t i = 0; i < ring.size(); i++) {
            vertices.emplace_back(ring[i][0]);
            vertices.emplace_back(ring[i][1]);
            vertices.emplace_back(ring[(i + 1) % ring.size()][0]);
            vertices.emplace_back(ring[(i + 1) % ring.size()][1]);
        }
    }

    // static constexpr uint32_t INDEX_DATA[] = {0, 1, 2};

    // static constexpr GLuint BUFFER_BINDING_INDEX = 0;


    glDeleteBuffers(1, &mVBO);
    glCreateBuffers(1, &mVBO);
    glNamedBufferStorage(mVBO, sizeof(vertices[0]) * vertices.size(), vertices.data(), GL_DYNAMIC_STORAGE_BIT);

    mLineCount = vertices.size() / 2 / 2;
    mElementCount = indices.size();

    glDeleteBuffers(1, &mEBO);
    glCreateBuffers(1, &mEBO);
    glNamedBufferStorage(mEBO, sizeof(indices[0]) * indices.size(), indices.data(), GL_DYNAMIC_STORAGE_BIT);

    if (!mVAO)
        glCreateVertexArrays(1, &mVAO); // default VAO

    // glVertexArrayVertexBuffer(mVAO, BUFFER_BINDING_INDEX, mVBO, 0, sizeof(float) * 6);
    glVertexArrayElementBuffer(mVAO, mEBO);

    // glEnableVertexArrayAttrib(mVAO, mShader.inPosition);
    // glEnableVertexArrayAttrib(mVAO, mShader.inColor);

    // glVertexArrayAttribFormat(mVAO, mShader.inPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 0);
    // glVertexArrayAttribFormat(mVAO, mShader.inColor, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3);

    // glVertexArrayAttribBinding(mVAO, mShader.inPosition, BUFFER_BINDING_INDEX);
    // glVertexArrayAttribBinding(mVAO, mShader.inColor, BUFFER_BINDING_INDEX);
}

RendererToSDF::~RendererToSDF() {
    glDeleteVertexArrays(1, &mVAO);
    glDeleteBuffers(1, &mVBO);
    glDeleteBuffers(1, &mEBO);
}

void RendererToSDF::renderPolygon(const multipolygon_type & multipolygon, float * VP) {
    clear();
    for (auto & polygon : multipolygon) {
        loadPolygon(polygon);
        draw(1.0f, VP);
    }
}

void RendererToSDF::clear() {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepthf(1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
}

void RendererToSDF::draw(float dt, float * VP) {
    // pass 1
    glBlendEquation(GL_MAX);
    glProgramUniformMatrix4fv(mShaderPass1.name(), mShaderPass1.uVP, 1, GL_FALSE, VP);
    glProgramUniform1f(mShaderPass1.name(), mShaderPass1.uDistanceRange, 0.05f); // TODO: in source coordinate system units
    glUseProgram(mShaderPass1.name());
    glBindVertexArray(mVAO);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, mShaderPass1.inVertexBuffer, mVBO);
    glDrawArrays(GL_TRIANGLES, 0, 6 * mLineCount);
    glUseProgram(0);
    // pass 2
    glBlendEquation(GL_FUNC_SUBTRACT);
    glProgramUniformMatrix4fv(mShaderPass2.name(), mShaderPass2.uVP, 1, GL_FALSE, VP);
    glUseProgram(mShaderPass2.name());
    glBindVertexArray(mVAO);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, mShaderPass2.inVertexBuffer, mVBO);
    glDrawElements(GL_TRIANGLES, mElementCount, GL_UNSIGNED_INT, 0);
    glUseProgram(0);
}
