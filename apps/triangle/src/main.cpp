#include <string>
#include <filesystem>
#include <chrono>
#include <vector>
#include <memory>
#include <array>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <fmt/format.h>
#include <nlohmann/json.hpp>

#include <Log.hpp>
#include <util.hpp>

#include "RendererBase.hpp"
#include "RendererTriangle.hpp"
#include "RendererToSDF.hpp"

#if 1
inline constexpr auto DEBUG_OPENGL = true;
inline constexpr auto DEBUG_GLFW = true;
inline constexpr auto SHOW_CMD = true;
#else
inline constexpr auto DEBUG_OPENGL = false;
inline constexpr auto DEBUG_GLFW = false;
inline constexpr auto SHOW_CMD = false;
#endif

inline constexpr auto V_SYNC = true;

inline constexpr int MSAA_SAMPLE_COUNT = 0; // 0 means no MSAA

void GLAPIENTRY theOpenglDebugMessageCallback(
    GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar * message,
    const void * userParam
) {
    switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH:
        log::error("OpenGL: {}", message);
        break;
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        log::info("OpenGL: {}", message);
        break;
    default:
        log::warning("OpenGL: {}", message);
        break;
    }
}

void theGlfwErrorCallback(int code, const char * description) {
    log::error("GLFW: {}", description);
}

std::pair<RendererToSDF::multipolygon_type, std::array<float, 16>> loadGeojson(const std::filesystem::path & path) {
    const auto json = nlohmann::json::parse(util::loadFile<char>(path));

    auto & features = json["features"];

    RendererToSDF::multipolygon_type mp;

    for (auto & feature : features) {
        auto & geometry = feature["geometry"];
        if (geometry["type"] == "MultiPolygon") {
            auto & multipolygon = geometry["coordinates"];
            for (auto & polygon : multipolygon) {
                mp.push_back(polygon);
                for (auto & ring : mp.back())
                    ring.pop_back(); // remove last element
            }
        } else if (geometry["type"] == "Polygon") {
            auto & polygon = geometry["coordinates"];
            mp.push_back(polygon);
            for (auto & ring : mp.back())
                ring.pop_back(); // remove last element
        }
    }

    float sx = 1.0f / 180.0f;
    float sy = 1.0f / 90.0f;
    float tx = 0.0f;
    float ty = 0.0f;

    return {
        mp,
        {
            sx,  0, 0, 0,
             0, sy, 0, 0,
             0,  0, 1, 0,
            tx, ty, 0, 1
        }
    };
}

int mainMain(int argc, char * argv[]) {
#ifdef _WIN32
    // ShowWindow(GetConsoleWindow(), SHOW_CMD ? SW_SHOW : SW_HIDE);
#endif

    nlohmann::json j;

    const auto glfwInitResult = glfwInit();
    if (glfwInitResult != GLFW_TRUE) return 0;

    if constexpr (DEBUG_GLFW) {
        glfwSetErrorCallback(theGlfwErrorCallback);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_NATIVE_CONTEXT_API);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_ROBUSTNESS, GLFW_LOSE_CONTEXT_ON_RESET);
    glfwWindowHint(GLFW_CONTEXT_RELEASE_BEHAVIOR, GLFW_RELEASE_BEHAVIOR_NONE);
    if constexpr (DEBUG_OPENGL) {
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
        glfwWindowHint(GLFW_CONTEXT_NO_ERROR, GLFW_FALSE);
    } else {
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_FALSE);
        glfwWindowHint(GLFW_CONTEXT_NO_ERROR, GLFW_TRUE);
    }
    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
    glfwWindowHint(GLFW_SAMPLES, MSAA_SAMPLE_COUNT);

    GLFWwindow * window = glfwCreateWindow(512, 512, "SDF", nullptr, nullptr);
    if (window == nullptr) return 0;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(V_SYNC ? 1 : 0);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        log::error("Failed to initialize OpenGL context");
        glfwDestroyWindow(window);
        glfwTerminate();
        window = nullptr;
        return 0;
    }

    if constexpr (MSAA_SAMPLE_COUNT > 0)
        glEnable(GL_MULTISAMPLE);
    else
        glDisable(GL_MULTISAMPLE);

    if constexpr (DEBUG_OPENGL) {
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(theOpenglDebugMessageCallback, nullptr);
    }

    GLint contextFlags = 0;
    glGetIntegerv(GL_CONTEXT_FLAGS, &contextFlags);

    log::info(
        "\n"
        "GLFW:\n"
        "  Version : {}.{}.{}\n"
        "OpenGL:\n"
        "  Version  : {}\n"
        "  GLSL     : {}\n"
        "  Vendor   : {}\n"
        "  Renderer : {}\n"
        "Context:\n"
        "  Forward Compatible: {}\n"
        "  Debug             : {}\n"
        "  Robust Access     : {}\n"
        "  No Error          : {}",
        GLFW_VERSION_MAJOR, GLFW_VERSION_MINOR, GLFW_VERSION_REVISION,
        glGetString(GL_VERSION),
        glGetString(GL_SHADING_LANGUAGE_VERSION),
        glGetString(GL_VENDOR),
        glGetString(GL_RENDERER),
        (contextFlags & GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT) != 0,
        (contextFlags & GL_CONTEXT_FLAG_DEBUG_BIT) != 0,
        (contextFlags & GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT) != 0,
        (contextFlags & GL_CONTEXT_FLAG_NO_ERROR_BIT) != 0
    );

    // glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

    auto [mp, transform] = loadGeojson("data/continents.json");

    {
        using clock_type = std::chrono::steady_clock;

        std::filesystem::path assetPath{"."};

        // std::vector<std::unique_ptr<RendererBase>> renderers;
        // renderers.emplace_back(std::make_unique<RendererTriangle>(assetPath));
        // renderers.emplace_back(std::make_unique<RendererToSDF>(assetPath));
        auto renderer = std::make_unique<RendererToSDF>(assetPath);

        auto currentTime = clock_type::now();

        while (!glfwWindowShouldClose(window)) {
            glfwPollEvents();

            const auto newCurrentTime = clock_type::now();
            const float dt = std::chrono::duration<float>{newCurrentTime - currentTime}.count();

            int frameBufferWidth = 0;
            int frameBufferHeight = 0;
            glfwGetFramebufferSize(window, &frameBufferWidth, &frameBufferHeight);
            glViewport(0, 0, frameBufferWidth, frameBufferHeight);

            glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glDisable(GL_CULL_FACE);

            renderer->renderPolygon(mp, transform.data());

            glfwSwapBuffers(window);
        }
    }

    if (window != nullptr) {
        glfwDestroyWindow(window);
    }
    glfwTerminate();

    return 0;
}

int main(int argc, char * argv[]) {
    mainMain(argc, argv);
}
