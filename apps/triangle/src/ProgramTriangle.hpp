#pragma once

#include "ProgramBase.hpp"

class ProgramTriangle : public ProgramBase {
public:
    static constexpr GLint uVP = 0;
    static constexpr GLint inPosition = 0;
    static constexpr GLint inColor = 1;

    void reload(const std::filesystem::path & assetPath) {
        ProgramBase::ShaderSourcePaths shaderSourcePaths;
        shaderSourcePaths.vert = assetPath / "shaders/triangle.vert";
        shaderSourcePaths.frag = assetPath / "shaders/triangle.frag";
        ProgramBase::reload(shaderSourcePaths, "ProgramTriangle");
    }

};
