#pragma once

#include <filesystem>

#include "RendererBase.hpp"
#include "ProgramTriangle.hpp"

class RendererTriangle : public RendererBase {
public:
    RendererTriangle(const std::filesystem::path & assetPath);
    ~RendererTriangle();
    void draw(float dt, float * VP) override;
private:
    ProgramTriangle mShader;
    GLuint mVAO = 0;
    GLuint mVBO = 0;
    GLuint mEBO = 0;
};
