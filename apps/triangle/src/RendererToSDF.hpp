#pragma once

#include <filesystem>
#include <array>
#include <vector>

#include <glad/glad.h>

#include "RendererBase.hpp"
#include "ProgramToSDFPass1.hpp"
#include "ProgramToSDFPass2.hpp"

class RendererToSDF {
public:
    RendererToSDF(const std::filesystem::path & assetPath);
    ~RendererToSDF();

    using vertex_type = std::array<float, 2>;
    using ring_type = std::vector<vertex_type>;
    using polygon_type = std::vector<ring_type>;
    using multipolygon_type = std::vector<polygon_type>;

    void renderPolygon(const multipolygon_type & multipolygon, float * VP);

private:
    ProgramToSDFPass1 mShaderPass1;
    ProgramToSDFPass2 mShaderPass2;
    GLuint mVAO = 0;
    GLuint mVBO = 0;
    GLuint mEBO = 0;
    GLsizei mLineCount = 0;
    GLsizei mElementCount = 0;

    void loadPolygon(
        const std::vector<std::vector<std::array<float, 2>>> & polygon
    );
    void draw(float dt, float * VP);
    void clear();
};
