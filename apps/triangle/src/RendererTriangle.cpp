#include "RendererTriangle.hpp"

RendererTriangle::RendererTriangle(
    const std::filesystem::path & assetPath
) {
    static constexpr float VERTEX_DATA[] = {
        -1.0f, -1.0f, 0.0f, 1.0, 0.0, 0.0,
         1.0f, -1.0f, 0.0f, 0.0, 1.0, 0.0,
         0.0f,  1.0f, 0.0f, 0.0, 0.0, 1.0,
    };

    static constexpr uint32_t INDEX_DATA[] = {0, 1, 2};

    static constexpr GLuint BUFFER_BINDING_INDEX = 0;

    mShader.reload(assetPath);

    glCreateBuffers(1, &mVBO);
    glNamedBufferStorage(mVBO, sizeof(VERTEX_DATA), VERTEX_DATA, GL_DYNAMIC_STORAGE_BIT);

    glCreateBuffers(1, &mEBO);
    glNamedBufferStorage(mEBO, sizeof(INDEX_DATA), INDEX_DATA, GL_DYNAMIC_STORAGE_BIT);

    glCreateVertexArrays(1, &mVAO);

    glVertexArrayVertexBuffer(mVAO, BUFFER_BINDING_INDEX, mVBO, 0, sizeof(float) * 6);
    glVertexArrayElementBuffer(mVAO, mEBO);

    glEnableVertexArrayAttrib(mVAO, mShader.inPosition);
    glEnableVertexArrayAttrib(mVAO, mShader.inColor);

    glVertexArrayAttribFormat(mVAO, mShader.inPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 0);
    glVertexArrayAttribFormat(mVAO, mShader.inColor, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3);

    glVertexArrayAttribBinding(mVAO, mShader.inPosition, BUFFER_BINDING_INDEX);
    glVertexArrayAttribBinding(mVAO, mShader.inColor, BUFFER_BINDING_INDEX);
}

RendererTriangle::~RendererTriangle() {
    glDeleteVertexArrays(1, &mVAO);
    glDeleteBuffers(1, &mVBO);
    glDeleteBuffers(1, &mEBO);
}

void RendererTriangle::draw(float dt, float * VP) {
    static constexpr float PROJECTION[] = {
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f,
    };
    glProgramUniformMatrix4fv(mShader.name(), mShader.uVP, 1, GL_FALSE, PROJECTION);
    glUseProgram(mShader.name());
    glBindVertexArray(mVAO);
    glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0);
    glUseProgram(0);
}
